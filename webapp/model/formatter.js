sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		delivery: function(sMeasure) {
			var sResult = "";

			if (sMeasure === 'created') {
				sResult = "Error";
			} else if (sMeasure === 'approved') {
				sResult = "Warning";
			} else if (sMeasure === 'execution') {
				sResult = "None";
			} else if (sMeasure === 'closed') {
				sResult = "Success";
			} else {
				sResult = "";
			}
			return sResult;
		},
		
		statusColor: function(sMeasure) {
			var sResult = "";

			if (sMeasure === '') {
				sResult = "#bfbfbf";
			} 
			/*else if (sMeasure === 'approved') {
				sResult = "Warning";
			} */
			/*else if (sMeasure === 'execution') {
				sResult = "None";
			} */
			else if (sMeasure === 'closed') {
				sResult = "#1dc159";
			} else {
				sResult = "#bb0000";
			}
			return sResult;
		}

	};
});