sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./TableExampleUtils",
	"sap/m/MessageToast",
	"sap/ui/core/routing/History"
], function(Controller, TableExampleUtils, MessageToast, History) {
	"use strict";

	return Controller.extend("PCP_HTS.controller.EGP_Dashboard_Alerts", {

		onInit: function() {
			// set explored app's demo model on this sample
			//var oJSONModel = TableExampleUtils.initSampleDataModel();
			//this.getView().setModel(oJSONModel);
			//var model = this.getView().getModel();
		},

		handleDetailsPress: function(oEvent) {
			MessageToast.show("Details for product with id " + this.getView().getModel().getProperty("ProductId", oEvent.getSource().getBindingContext()));
		},

		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		createNew: function() {
			this.getView().unbindElement();
			this.getRouter().navTo("wizard");
		},

		goBack: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("overview", true);
			}

		}

	});

});