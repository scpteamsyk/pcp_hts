sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History"
], function(Controller, History) {
	"use strict";

	return Controller.extend("PCP_HTS.controller.EGP_Dashboard_Console", {

		onInit: function() {

			var model = this.getOwnerComponent().getModel("services");
			setInterval(function() {
				model.refresh();
				//jQuery.sap.log.error("timer");
			}, 5000);

		},

		/*		goBack: function() {
					this.getView().unbindElement();
					this.getRouter().navTo("country");
				},*/

		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		}

	});
});