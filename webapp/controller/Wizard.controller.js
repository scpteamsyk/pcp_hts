sap.ui.define([
	'jquery.sap.global',
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageToast",
	'sap/ui/core/Fragment',
	"sap/m/MessageBox"
], function(jQuery, Controller, JSONModel, MessageToast, Fragment, MessageBox) {
	"use strict";

	var te = "Anthony";
	var issueID = 0;

	var WizardController = Controller.extend("PCP_HTS.controller.Wizard", {
		onInit: function() {
			this._wizard = this.getView().byId("CreateProductWizard");
			this._oNavContainer = this.getView().byId("wizardNavContainer");
			this._oWizardContentPage = this.getView().byId("wizardContentPage");
			this._oWizardReviewPage = sap.ui.xmlfragment("pcp_hts.fragment.ReviewPage", this);

			this._oNavContainer.addPage(this._oWizardReviewPage);
			this.model = new sap.ui.model.json.JSONModel();
			this.model.setData({
				productNameState: "Error",
				productWeightState: "Error"
			});
			this.getView().setModel(this.model);
			this.model.setProperty("/productType", "Mobile");
			this.model.setProperty("/navApiEnabled", true);
			this.model.setProperty("/productVAT", false);
			this._setEmptyValue("/productManufacturer");
			this._setEmptyValue("/productDescription");
			this._setEmptyValue("/productPrice");

			issueID = Math.floor(Math.random() * 1000000);
			//this.getView().byId("ProductTypeStep").setTitle("Title Issue N°" + issueID);

		},
		setProductType: function(evt) {
			var productType = evt.getSource().getTitle();
			this.model.setProperty("/productType", productType);
			this.getView().byId("ProductStepChosenType").setText("Chosen product type: " + productType);
			this._wizard.validateStep(this.getView().byId("ProductTypeStep"));
		},
		setProductTypeFromSegmented: function(evt) {
			/*	var productType = evt.mParameters.button.getText();
				this.model.setProperty("/productType", productType);
				this._wizard.validateStep(this.getView().byId("ProductTypeStep"));*/
		},
		additionalInfoValidation: function() {
			var name = this.getView().byId("ProductName").getValue();
			var weight = parseInt(this.getView().byId("ProductWeight").getValue());

			//isNaN(weight) ? this.model.setProperty("/productWeightState", "Error") : this.model.setProperty("/productWeightState", "None");
			//name.length<6 ?  this.model.setProperty("/productNameState", "Error") : this.model.setProperty("/productNameState", "None");

			if (name.length < 6 || isNaN(weight)) {
				this._wizard.invalidateStep(this.getView().byId("ProductInfoStep"));
			} else {
				this._wizard.validateStep(this.getView().byId("ProductInfoStep"));
			}
		},
		optionalStepActivation: function() {},
		optionalStepCompletion: function() {},
		pricingActivate: function() {
			this.model.setProperty("/navApiEnabled", true);
		},
		pricingComplete: function() {
			this.model.setProperty("/navApiEnabled", false);
		},
		scrollFrom4to2: function() {
			this._wizard.goToStep(this.getView().byId("ProductInfoStep"));
		},
		goFrom4to3: function() {
			if (this._wizard.getProgressStep() === this.getView().byId("PricingStep")){
				this._wizard.previousStep();
			}
		},
		goFrom4to5: function() {
			if (this._wizard.getProgressStep() === this.getView().byId("PricingStep")){
				this._wizard.nextStep();
			}
		},
		wizardCompletedHandler: function() {
			this.fullfillReviewPageWithData();
			this._oNavContainer.to(this._oWizardReviewPage);
		},
		fullfillReviewPageWithData: function() {
			sap.ui.getCore().byId("plantid").setText(this.getView().byId("__label13").getText());
			sap.ui.getCore().byId("funcloc").setText(this.getView().byId("__label19").getText());
			sap.ui.getCore().byId("compid").setText(this.getView().byId("__label15").getText());
			sap.ui.getCore().byId("plantcap").setText(this.getView().byId("__label17").getText());
			sap.ui.getCore().byId("pdesc").setText(this.getView().byId("__area1").getValue());
			sap.ui.getCore().byId("probtype").setText(this.getView().byId("__item0").getText());
			sap.ui.getCore().byId("probcat").setText(this.getView().byId("__item1").getText());
			sap.ui.getCore().byId("probdesc").setText(this.getView().byId("pdarea").getValue());
			sap.ui.getCore().byId("specprob").setText(this.getView().byId("__item2").getText());
			sap.ui.getCore().byId("probdate").setText(this.getView().byId("dofdet").getDateValue());
			//sap.ui.getCore().byId("probpr").setText(this.getView().byId("__label1222").getText());
		},
		backToWizardContent: function() {
			this._oNavContainer.backToPage(this._oWizardContentPage.getId());
		},
		editStepOne: function() {
			this._handleNavigationToStep(0);
		},
		editStepTwo: function() {
			this._handleNavigationToStep(1);
		},
		editStepThree: function() {
			this._handleNavigationToStep(2);
		},
		editStepFour: function() {
			this._handleNavigationToStep(3);
		},
		_handleNavigationToStep: function(iStepNumber) {
			var that = this;

			function fnAfterNavigate() {
				that._wizard.goToStep(that._wizard.getSteps()[iStepNumber]);
				that._oNavContainer.detachAfterNavigate(fnAfterNavigate);
			}

			this._oNavContainer.attachAfterNavigate(fnAfterNavigate);
			this.backToWizardContent();
		},
		_handleMessageBoxOpen: function(sMessage, sMessageBoxType) {
			var that = this;
			MessageBox[sMessageBoxType](sMessage, {
				actions: [MessageBox.Action.YES, MessageBox.Action.NO],
				onClose: function(oAction) {
					if (oAction === MessageBox.Action.YES) {
						that._handleNavigationToStep(0);
						//that._wizard.discardProgress(that._wizard.getSteps()[0]);
						that.discardProgress();
						// faccio la post della issue
						//that.postIssue();
					}
					that.getView().unbindElement();
					that.getRouter().navTo("detail");
				}
			});
		},
		deleteTables: function() {
			var xmlHttp = new XMLHttpRequest();
			xmlHttp.open("GET", "//pcphanamdcs0016748836trial.hanatrial.ondemand.com/pcp/services/pcpdelete.xsjs", false); // false for synchronous request
			xmlHttp.withCredentials = true;
			xmlHttp.send(null);
			return xmlHttp.responseText;
		},
		postIssue: function() {
			this.deleteTables();
			var model = this.getOwnerComponent().getModel("services");
			var newEntry = {};

			var date = sap.ui.getCore().byId("probdate").getText();
			var minDate = date.substring(4, 21);

			newEntry.ISSUE_ID = "" + issueID;
			newEntry.PLANTID = sap.ui.getCore().byId("plantid").getText();
			newEntry.FUNCTIONAL_LOCATION_GU = sap.ui.getCore().byId("funcloc").getText();
			newEntry.COMPONENT_ID = sap.ui.getCore().byId("compid").getText();
			newEntry.PLANT_CAPACITY = sap.ui.getCore().byId("plantcap").getText();
			newEntry.DESCRIPTION = sap.ui.getCore().byId("pdesc").getText();
			newEntry.PROBLEM_TYPE = sap.ui.getCore().byId("probtype").getText();
			newEntry.PROBLEM_CATEGORY = sap.ui.getCore().byId("probcat").getText();
			newEntry.PROBLEM_DESCRIPTION = sap.ui.getCore().byId("probdesc").getText();
			newEntry.SPECIFIC_PROBLEM = sap.ui.getCore().byId("specprob").getText();
			newEntry.DETECTION_DATE = minDate;
			newEntry.ALARM_LOG = "";
			newEntry.PRIORITY = sap.ui.getCore().byId("probpr").getText();
			newEntry.TICKET_ID = "";
			newEntry.TICKET_STATUS = "open";
			newEntry.NOTIFICATION_ID = "";
			newEntry.NOTIFICATION_STATUS = "";
			newEntry.ORDER = "";
			newEntry.ORDER_STATUS = "";
			newEntry.USER_ID = "";
			newEntry.USER_LASTNAME = "Anthony";
			newEntry.OPEN_DATE = "";
			newEntry.CLOSE_DATE = "";
			newEntry.STATUS = "open";

			model.create("/issue", newEntry, null, function(oData) {}, function(error) {});
			model.refresh();
		},
		_setEmptyValue: function(sPath) {
			this.model.setProperty(sPath, "n/a");
		},
		handleWizardCancel: function() {
			this._handleMessageBoxOpen("Are you sure you want to cancel your report?", "warning");
		},
		handleWizardSubmit: function() {
			this._handleMessageBoxOpen("Are you sure you want to submit your report?", "confirm");
		},
		productWeighStateFormatter: function(val) {
			return isNaN(val) ? "Error" : "None";
		},
		discardProgress: function() {
			this._wizard.discardProgress(this.getView().byId("ProductTypeStep"));

			var clearContent = function(content) {
				for (var i = 0; i < content.length; i++) {
					if (content[i].setValue) {
						content[i].setValue(sap.ui.getCore().byId(""));
					}

					if (content[i].getContent) {
						clearContent(content[i].getContent());
					}
				}
			};

			this.model.setProperty("/productWeightState", "Error");
			this.model.setProperty("/productNameState", "Error");
			clearContent(this._wizard.getSteps());
		},
		// gestisco popup scelta technical expert
		handleLinkPress: function(oEvent) {
			// create popover
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("pcp_hts.fragment.ExpertPopover", this);
				this.getView().addDependent(this._oPopover);
				//this._oPopover.bindElement("/ProductCollection/0");
			}

			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function() {
				this._oPopover.openBy(oButton);
			});
		},
		choosePress: function(oEvent) {
			this._oPopover.close();
			MessageToast.show("Technical Expert chosen");
		},
		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},
		handleWizardEnd: function() {
			this._handleNavigationToStep(0);
			this._wizard.discardProgress(this._wizard.getSteps()[0]);
			this.discardProgress();
			this.postIssue();
			this.prepareForNextWizard();
			this.getView().unbindElement();
			this.getRouter().navTo("detail");
		},
		handleWizardEndCancel: function() {
			this._handleNavigationToStep(0);
			this._wizard.discardProgress(this._wizard.getSteps()[0]);
			this.getView().unbindElement();
			this.getRouter().navTo("detail");
		},
		prepareForNextWizard: function() {
			this.getView().byId("__area1").setValue("");
			this.getView().byId("pdarea").setValue("");
			issueID = Math.floor(Math.random() * 1000000);
			//this.getView().byId("ProductTypeStep").setTitle("Title Issue N°" + issueID);
		}
	});

	return WizardController;
});