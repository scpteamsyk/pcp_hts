sap.ui.define(["sap/ui/vbm/AnalyticMap", "sap/m/MessageToast", "sap/ui/core/mvc/Controller"],
	function(AnalyticMap, MessageToast, Controller) {
		"use strict";
		AnalyticMap.GeoJSONURL = "./json/L0.json";
		return Controller.extend("PCP_HTS.controller.EGP_Dashboard", {
			onInit: function() {

				var model = this.getOwnerComponent().getModel("services");
				setInterval(function() {
					model.refresh();
					//jQuery.sap.log.error("timer");
				}, 5000);
			  
			},
			getRouter: function() {
				return sap.ui.core.UIComponent.getRouterFor(this);
			},

			onRegionClick: function(e) {
				if (e.getParameter("code") === "US") {
					this.getView().unbindElement();
					this.getRouter().navTo("country");
					
				}
			}
			
		});

	});