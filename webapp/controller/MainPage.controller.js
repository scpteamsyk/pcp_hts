sap.ui.define(['sap/m/MessageToast', 'sap/ui/core/mvc/Controller'],
	function(MessageToast, Controller) {
		"use strict";

		return Controller.extend("PCP_HTS.controller.MainPage", {

			getRouter: function() {
				return sap.ui.core.UIComponent.getRouterFor(this);
			},

			onPress: function(evt) {
				this.getView().unbindElement();
				this.getRouter().navTo("detail");
			},
			onAlarms: function (oEvent) {
			
			this.getRouter().navTo("al");
		}
		});

	});