sap.ui.define(['sap/ui/core/mvc/Controller',
		'sap/ui/model/json/JSONModel'
	],
	function(Controller, JSONModel) {
		"use strict";

		var PageController = Controller.extend("PCP_HTS.controller.EGP_Plant", {

			onInit: function() {
				// create model
				var oModel = new JSONModel();
				oModel.setData({
					startDate: new Date("2017", "07", "07", "8", "0"),
					people: [{
							appointments: [{
								start: new Date("2017", "07", "01", "08", "30"),
								end: new Date("2017", "07", "10", "09", "30"),
								title: " ",
								info: " ",
								type: "Type01",
								pic: "sap-icon://sap-ui5",
								tentative: false
							}, {
								start: new Date("2017", "07", "07", "10", "0"),
								end: new Date("2017", "07", "20", "12", "0"),
								title: " ",
								info: " ",
								type: "Type04",
								pic: "sap-icon://sap-ui5",
								tentative: false
							}, {
								start: new Date("2017", "07", "18", "11", "30"),
								end: new Date("2017", "07", "25", "13", "30"),
								title: "Lunch",
								info: "room 3",
								type: "Type04",
								pic: "sap-icon://sap-ui5",
								tentative: true
							}]
						}

					]
				});
				this.getView().setModel(oModel);

			},

			getRouter: function() {
				return sap.ui.core.UIComponent.getRouterFor(this);
			},

			goToWorkOrders: function() {
				this.getView().unbindElement();
				this.getRouter().navTo("wo");
			},

			goToAlert: function() {
				this.getView().unbindElement();
				this.getRouter().navTo("al");
			},

			goToIssues: function() {
				this.getView().unbindElement();
				this.getRouter().navTo("issu");
			}

		});

		return PageController;

	});