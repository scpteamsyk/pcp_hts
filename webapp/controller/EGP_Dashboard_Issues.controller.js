sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"./TableExampleUtils",
	"sap/m/MessageToast",
	"sap/ui/core/routing/History",
	"../model/formatter"
], function(Controller, TableExampleUtils, MessageToast, History, formatter) {
	"use strict";

	var tse = "";
	var technician = "";

	return Controller.extend("PCP_HTS.controller.EGP_Dashboard_Issues", {

		onInit: function() {
			// set explored app's demo model on this sample
			var oJSONModel = TableExampleUtils.initSampleDataModel();
			this.getView().setModel(oJSONModel);
		},

		formatter: formatter,

		handleDetailsPress: function(oEvent) {
			MessageToast.show("Details for product with id " + this.getView().getModel().getProperty("ProductId", oEvent.getSource().getBindingContext()));
		},

		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		},

		createNew: function() {
			this.getView().unbindElement();
			this.getRouter().navTo("wizard");
		},

		handleLinkPress: function(oEvent) {
			// create popover
			if (!this._tPopover) {
				this._tPopover = sap.ui.xmlfragment("pcp_hts.fragment.ExpertPopover", this);
				this.getView().addDependent(this._tPopover);
				//this._oPopover.bindElement("/ProductCollection/0");
			}

			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function() {
				this._tPopover.openBy(oButton);
			});
		},

		handleLinkPress1: function(oEvent) {
			// create popover
			if (!this._ttPopover) {
				this._ttPopover = sap.ui.xmlfragment("pcp_hts.fragment.TechnicalPopover", this);
				this.getView().addDependent(this._ttPopover);
				//this._oPopover.bindElement("/ProductCollection/0");
			}

			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function() {
				this._ttPopover.openBy(oButton);
			});
		},

		choosePress: function(oEvent) {
			var fragmentId = sap.ui.xmlfragment("pcp_hts.fragment.ExpertPopover", this).getId();
			var list = sap.ui.core.Fragment.byId(fragmentId, "list");
			var items = list.getSelectedItems();
			var expert = items[0].getTitle();
			MessageToast.show("Technical Expert chosen" + expert);
		},

		showDetail: function(oEvent) {
			this.populateDetail();
			if (!this._pPopover) {
				this._pPopover = sap.ui.xmlfragment("pcp_hts.fragment.IssueDetail", this);
				this.getView().addDependent(this._pPopover);
				//this._oPopover.bindElement("/ProductCollection/0");
			}

			// delay because addDependent will do a async rerendering and the actionSheet will immediately close without it.
			var oButton = oEvent.getSource();
			jQuery.sap.delayedCall(0, this, function() {
				this._pPopover.openBy(oButton);
			});
		},

		populateDetail: function() {
			var model = this.getOwnerComponent().getModel("services");

			model.read("/issue", {
				success: function(oData) {
					if (oData.results.length !== 0) {
						var oIssue = new sap.ui.model.json.JSONModel(oData.results[0]);
						sap.ui.getCore().byId("ltxt").setText(oIssue.oData.ISSUE_ID);
						sap.ui.getCore().byId("ltxt1").setText(oIssue.oData.PLANTID);
						sap.ui.getCore().byId("ltxt2").setText(oIssue.oData.FUNCTIONAL_LOCATION_GU);
						sap.ui.getCore().byId("ltxt3").setText(oIssue.oData.COMPONENT_ID);
						sap.ui.getCore().byId("ltxt4").setText(oIssue.oData.PLANT_CAPACITY);
						sap.ui.getCore().byId("ltxt5").setText(oIssue.oData.DESCRIPTION);
						sap.ui.getCore().byId("ltxt6").setText(oIssue.oData.PROBLEM_TYPE);
						sap.ui.getCore().byId("ltxt7").setText(oIssue.oData.PROBLEM_CATEGORY);
						sap.ui.getCore().byId("ltxt8").setText(oIssue.oData.PROBLEM_DESCRIPTION);
						sap.ui.getCore().byId("ltxt9").setText(oIssue.oData.SPECIFIC_PROBLEM);
						sap.ui.getCore().byId("ltxt10").setText(oIssue.oData.DETECTION_DATE);
						sap.ui.getCore().byId("ltxt11").setText(oIssue.oData.ALARM_LOG);
						sap.ui.getCore().byId("ltxt12").setText(oIssue.oData.PRIORITY);
					}
				}

			});
		},

		tseSelection: function(evt) {
			var boxId = evt.getSource().getId();
			if (boxId === "rrb1") {
				//this.getView().byId("rrb2").setSelected(false);
				//this.getView().byId("rrb3").setSelected(false);
				tse = "CARR";
			} else if (boxId === "rrb2") {
				//this.getView().byId("rrb1").setSelected(false);
				//this.getView().byId("rrb3").setSelected(false);
				tse = "JOHNSON";
			} else if (boxId === "rrb3") {
				//this.getView().byId("rrb1").setSelected(false);
				//this.getView().byId("rrb2").setSelected(false);
				tse = "STANS";
			}
		},

		techSelection: function(evt) {
			var boxId = evt.getSource().getId();
			if (boxId === "rb1") {
				//this.getView().byId("rb2").setSelected(false);
				//this.getView().byId("rb3").setSelected(false);
				technician = "JOHNES";
			} else if (boxId === "rb2") {
				//this.getView().byId("rb1").setSelected(false);
				//this.getView().byId("rb3").setSelected(false);
				technician = "SMITH";
			} else if (boxId === "rb3") {
				//this.getView().byId("rb1").setSelected(false);
				//this.getView().byId("rb2").setSelected(false);
				technician = "JACKSON";
			}
		},

		userSelection: function() {
			this.showBusyIndicator(4000);
			var model = this.getOwnerComponent().getModel("services");
			if (!this._tPopover) {
				this._tPopover = sap.ui.xmlfragment("pcp_hts.fragment.ExpertPopover", this);
				this.getView().addDependent(this._tPopover);
				//this._oPopover.bindElement("/ProductCollection/0");
			}

			var tsePopover = this._tPopover;
			model.read("/issue", {
				success: function(oData) {
					if (oData.results.length !== 0) {
						var oIssue = new sap.ui.model.json.JSONModel(oData.results[0]);
						var issueID = oIssue.oData.ISSUE_ID;
						var path = "/issue(ISSUE_ID='" + issueID + "')";
						var newEntry = {};
						newEntry.TSUSER_LASTNAME = tse;

						var ticketId = Math.floor(Math.random() * 1000000);
						newEntry.STATUS = "tsa assigned";
						newEntry.TICKET_STATUS = "open";
						newEntry.TICKET_ID = "" + ticketId;

						model.update(path, newEntry, null, function(oData) {}, function(error) {});
						model.refresh();
						//this.getView().byId("tsabtn").enabled(true);
					}

				}

			});
			tsePopover.close();
		},
		
		hideBusyIndicator : function() {
			sap.ui.core.BusyIndicator.hide();
		},

		showBusyIndicator: function(iDuration, iDelay) {
			sap.ui.core.BusyIndicator.show(iDelay);

			if (iDuration && iDuration > 0) {
				if (this._sTimeoutId) {
					jQuery.sap.clearDelayedCall(this._sTimeoutId);
					this._sTimeoutId = null;
				}

				this._sTimeoutId = jQuery.sap.delayedCall(iDuration, this, function() {
					this.hideBusyIndicator();
				});
			}
		},

		tuserSelection: function() {
			var model = this.getOwnerComponent().getModel("services");
			if (!this._ttPopover) {
				this._ttPopover = sap.ui.xmlfragment("pcp_hts.fragment.TechnicalPopover", this);
				this.getView().addDependent(this._ttPopover);
				//this._oPopover.bindElement("/ProductCollection/0");
			}

			var techPopover = this._ttPopover;

			model.read("/issue", {
				success: function(oData) {
					if (oData.results.length !== 0) {
						var oIssue = new sap.ui.model.json.JSONModel(oData.results[0]);
						var issueID = oIssue.oData.ISSUE_ID;
						var path = "/issue(ISSUE_ID='" + issueID + "')";
						var newEntry = {};
						newEntry.USER_LASTNAME = technician;

						var notId = Math.floor(Math.random() * 1000000);
						newEntry.STATUS = "technician assigned";
						newEntry.NOTIFICATION_STATUS = "open";
						newEntry.NOTIFICATION_ID = "" + notId;

						model.update(path, newEntry, null, function(oData) {}, function(error) {});
						model.refresh();
					}

				}

			});
			techPopover.close();
		},

		goBack: function() {
			var oHistory = History.getInstance();
			var sPreviousHash = oHistory.getPreviousHash();

			if (sPreviousHash !== undefined) {
				window.history.go(-1);
			} else {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("overview", true);
			}
		},

		closePress: function() {
			
			if (!this._pPopover) {
				this._pPopover = sap.ui.xmlfragment("pcp_hts.fragment.IssueDetail", this);
				this.getView().addDependent(this._pPopover);
				//this._oPopover.bindElement("/ProductCollection/0");
			}

			var detailPopover = this._pPopover;


			detailPopover.close();

		}

	});

});