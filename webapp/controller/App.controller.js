sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	'sap/ui/model/json/JSONModel'
], function(Controller, History, JSONModel) {
	"use strict";

	return Controller.extend("PCP_HTS.controller.App", {

		onInit: function() {
			//this.getRouter().navTo("Map");

			/*var oModel = new sap.ui.model.json.JSONModel();
			var aData = jQuery.ajax({
				type: "GET",
				contentType: "application/json",
				url: "/services/userapi",
				dataType: "json",
				async: false,
				success: function(data, textStatus, jqXHR) {
					oModel.setData({
						modelData: data
					});
					//alert("success to post");
				}

			});

			return oModel;*/

		},

		getRouter: function() {
			return sap.ui.core.UIComponent.getRouterFor(this);
		}
	});

});