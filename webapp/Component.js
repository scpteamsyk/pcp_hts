sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"PCP_HTS/model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("PCP_HTS.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			//this.setModel(models.createDeviceModel(), "device");
			//this.getTargets().display("page1");
			this.getRouter().initialize();

			var oRootPath = jQuery.sap.getModulePath("PCP_HTS"); // your resource root

			var oRootPathModel = new sap.ui.model.json.JSONModel({
				path: oRootPath
			});

			this.setModel(oRootPathModel, "rootPath");
		}
	});

});